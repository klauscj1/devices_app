class DeviceModel {
  DeviceModel({
    required this.id,
    required this.state,
    required this.name,
    required this.imagePath,
  });
  final int id;
  final String state;
  final String name;
  final String imagePath;
}

List<DeviceModel> devices = [
  DeviceModel(
    id: 1,
    imagePath: 'assets/images/googlehome.png',
    name: 'Google home',
    state: 'New device',
  ),
  DeviceModel(
    id: 2,
    imagePath: 'assets/images/googlehome.png',
    name: 'Amazon Eco',
    state: 'Primary device',
  ),
  DeviceModel(
    id: 3,
    imagePath: 'assets/images/googlehome.png',
    name: 'Amazon Eco Studio',
    state: 'Primary device',
  ),
];
