import 'package:devices_app/app/ui/theme/app_colors.dart';
import 'package:flutter/material.dart';

class Background extends StatelessWidget {
  const Background({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          color: AppColors.color2.withOpacity(.2),
        ),
      ],
    );
  }
}
