import 'package:flutter/material.dart';

class UserAvatar extends StatelessWidget {
  const UserAvatar({
    Key? key,
    required this.sizeAvatar,
  }) : super(key: key);

  final double sizeAvatar;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      right: 30,
      top: 50,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(100),
        child: Image.asset(
          'assets/images/user.jpg',
          fit: BoxFit.cover,
          width: sizeAvatar,
          height: sizeAvatar,
        ),
      ),
    );
  }
}
