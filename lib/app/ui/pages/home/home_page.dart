import 'package:devices_app/app/domain/models/device_model.dart';
import 'package:devices_app/app/ui/global_widget/backgroud_widget.dart';
import 'package:devices_app/app/ui/global_widget/user_avatar.dart';
import 'package:flutter/material.dart';

import 'widgets/card_widget.dart';
import 'widgets/custom_navbar.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          const Background(),
          const UserAvatar(
            sizeAvatar: 40,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Hello.',
                  style: Theme.of(context).textTheme.headline4!.copyWith(
                        color: Colors.grey.withOpacity(.7),
                        fontWeight: FontWeight.w300,
                      ),
                ),
                Text(
                  'How can I help?',
                  style: Theme.of(context).textTheme.headline4!.copyWith(
                        color: Colors.grey.withOpacity(.7),
                        fontWeight: FontWeight.w300,
                      ),
                ),
                const SizedBox(
                  height: 40,
                ),
                SizedBox(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * .4,
                  child: PageView.builder(
                    itemCount: devices.length,
                    itemBuilder: (_, index) {
                      return CardWidget(
                        device: devices[index],
                      );
                    },
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 40.0),
                  child: CustomNavbar(),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
