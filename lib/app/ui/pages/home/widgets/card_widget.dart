import 'package:devices_app/app/domain/models/device_model.dart';
import 'package:devices_app/app/ui/global_widget/custom_button.dart';
import 'package:devices_app/app/ui/theme/app_colors.dart';
import 'package:flutter/material.dart';

class CardWidget extends StatelessWidget {
  const CardWidget({
    Key? key,
    required this.device,
  }) : super(key: key);
  final DeviceModel device;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
          ),
        ),
        Positioned(
          right: 10,
          top: 10,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: AppColors.color2,
            ),
            width: 25,
            height: 25,
            child: const Center(
              child: Icon(
                Icons.close,
                size: 15,
              ),
            ),
          ),
        ),
        Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: Text(
                device.state,
                style: Theme.of(context).textTheme.caption,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Text(
                device.name,
                style: Theme.of(context).textTheme.headline6,
              ),
            ),
            Expanded(
              child: SizedBox(
                child: Image.asset(device.imagePath),
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              child: CustomButton(),
            ),
            const SizedBox(
              height: 20,
            )
          ],
        )
      ],
    );
  }
}
