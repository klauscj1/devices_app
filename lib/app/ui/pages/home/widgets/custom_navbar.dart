import 'package:devices_app/app/ui/theme/app_colors.dart';
import 'package:flutter/material.dart';

class CustomNavbar extends StatelessWidget {
  const CustomNavbar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        IconButton(
          onPressed: () {},
          icon: const Icon(Icons.blur_on_sharp),
        ),
        CircleAvatar(
          backgroundColor: Colors.white.withOpacity(.7),
          radius: 30,
          child: CircleAvatar(
            backgroundColor: AppColors.color3,
            radius: 27,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: Image.asset(
                'assets/images/icono2.png',
                fit: BoxFit.contain,
              ),
            ),
          ),
        ),
        IconButton(
          onPressed: () {},
          icon: const Icon(Icons.add_circle),
        ),
      ],
    );
  }
}
