import 'package:devices_app/app/ui/global_widget/user_avatar.dart';
import 'package:flutter/material.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: UserAvatar(
          sizeAvatar: 60,
        ),
      ),
    );
  }
}
