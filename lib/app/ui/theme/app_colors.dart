import 'package:flutter/material.dart';

class AppColors {
  static const Color color1 = Color(0xFFFFFFFF);
  static const Color color2 = Color(0xFFD4EAF0);
  static const Color color3 = Color(0xFFE8DFF2);
  static const Color color4 = Color(0xFF000000);
}
